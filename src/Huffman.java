

import java.util.*;

/**
 * Prefix codes and Huffman tree.
 * Tree depends on source data.
 * NB! lahenduse puhul on kasutatud endise ItCollege tudengi abimaterjale: http://enos.itcollege.ee/~ylari/I231/Huffman.java
 * Kuid lahendused on kirjutatud käsitsi.
 *
 */
public class Huffman {

   private int[] sagedused;
   private static final int TÄHESTIKU_PIKKUS=256;

   // Tabel vastavuste tekitanisesks
   private HashMap<Byte,Leaf> vastavusTabel;

   //
   private int length;

   /**
    *
    * @param original source data.
    */

   //Constructor to build the Huffman code for a given bytearray.

   Huffman(byte[] original) {
      sagedused=buildFrequencyTable(original);
      vastavusTabel = new HashMap<Byte,Leaf>();
      PriorityQueue<Tree>priorityQueue=new PriorityQueue<>();
      for(char i=0;i<TÄHESTIKU_PIKKUS;i++){
          // lisame listi ainult need tähed, millel on sagedus
         if(buildFrequencyTable(original)[i]>0){
            priorityQueue.offer(new Leaf(buildFrequencyTable(original)[i],(byte) i));}
      }
      while(priorityQueue.size()>1){
         final Tree left=priorityQueue.poll();
         final Tree right=priorityQueue.poll();
         final Node parent=new Node(left,right);
         priorityQueue.add(parent);
      }
      Tree tree = priorityQueue.poll();
      huffStruct(tree, new StringBuffer());


   }

//meetod, mis tekitab sagesuste tabeli
   public int[]buildFrequencyTable(byte [] origData){
      sagedused=new int[TÄHESTIKU_PIKKUS];
      for(byte origDatum : origData) {

         sagedused[origDatum]++;

      }

   return sagedused;}


      /**
       * Length of encoded data in bits.
       * @return number of bits.
       */
   public int bitLength() {
      return length;
   }

   /**
    * Encoding the byte array using this prefixcode.
    * @param origData original data.
    * @return encoded data.
    */
   public byte[] encode(byte[] origData) {
      StringBuffer puff = new StringBuffer(8);
      // lisame StingBuffrisse köik baidimassiivi elemendid
      for (byte bit : origData)
         puff.append(vastavusTabel.get(bit).name);
      length = puff.length();


      List<Byte> bitid = new ArrayList<Byte>();
// käime bufri läbi ning küik, konverdime 8biti kaupa ning lisame bitilisti ning ülejäävad elemendid konverteerime 0-dega
// 8 biti sammuseadmine
      while (puff.length()>0) {
           while(puff.length()<8)
                  puff.append('0');
                   String str = puff.substring(0, 8);
                  bitid.add((byte) Integer.parseInt(str, 2));
                  puff.delete(0, 8); }



      byte[] ret = new byte[bitid.size()];
      Iterator<Byte>it=bitid.iterator();
      int i=0;
      while(it.hasNext()){
         ret[i]=it.next().byteValue();
         i++;
      }
      return ret;

   }

   /**
    * Decoding the byte array using this prefixcode.
    * @param encodedData encoded data.
    * @return decoded data (hopefully identical to original).
    */
   public byte[] decode(byte[] encodedData) {
      //lahtipakkimiseks tuleb biti haaval puus üleval alla käia kuni tuleb leht, siis korrata.

      StringBuilder builder = new StringBuilder();
      List<Byte> pakkimata = new ArrayList<Byte>();

      //teeme bitid stringideks
      for (int i = 0; i < encodedData.length; i++)
         builder.append(byteToString8Bits(encodedData[i]));
      String ehit = builder.substring(0, this.length);

      StringBuffer puhver=new StringBuffer();



      for(int i=ehit.length();i>0;i--){
         puhver.append(ehit.substring(0, 1));
         ehit = ehit.substring(1);


         for(Iterator<Leaf> nimekiri = vastavusTabel.values().iterator();nimekiri.hasNext();){
            Leaf leht = nimekiri.next();
            if (leht.name.equals(puhver.toString())) {
               pakkimata.add(leht.value);
               puhver.setLength(0);
               break;
            } }

      }



      byte[] result = new byte[pakkimata.size()];
      int i = 0;
      for(Byte b:pakkimata){
         result[i++] = b;
      }

      return result;

   }



   private String byteToString8Bits(byte bits) {
      StringBuffer result = new StringBuffer(Integer.toBinaryString(bits));
      while (result.length() < 8) {
         result.insert(0, "0");
      }
      return result.substring(result.length() - 8, result.length()).toString();
   }


    private void huffStruct (Tree tree, StringBuffer prefix) {
        //Leaf on ilma lasteta puuharu,st et tema väärtus salvestub
        if (tree instanceof Leaf) {
            Leaf leaf = (Leaf) tree;
            leaf.name = (prefix.length() > 0) ? prefix.toString() : "0";
            vastavusTabel.put(leaf.value, leaf);
        }

        // kui haru on Node või tal on lapsi läheb vasakule 0 ja paremale 1 eesliide
        else if (tree instanceof Node) {
            Node node = (Node) tree;

            prefix.append('0');
            huffStruct(node.leftChild, prefix);
            prefix.deleteCharAt(prefix.length() - 1);

            prefix.append('1');
            huffStruct(node.rightChild, prefix);
            prefix.deleteCharAt(prefix.length() - 1);
        }
    }




   // Huffmani puu

   public class Tree implements Comparable<Tree> {
      /** The frequency of this tree. */
      public final int frequency;
      public Tree(int frequency) {
         this.frequency = frequency;
      }

      /**
       * This is an overriding method.
       * @see java.lang.Comparable#compareTo(java.lang.Object)
       * {@inheritDoc}
       */
      @Override
      public int compareTo(Tree o) {
         return frequency - o.frequency;
      }

   }

   // huffmani puu haru, millel ei ole nö järglasi
   class Leaf extends Tree {
       /** The byte this leaf represents. */
      public final byte value;
      public String name;

      public Leaf(int frequency, byte value) {
          //pärime sageduse
         super(frequency);
         this.value = value;
      }

   }

   //Huffmani puuharu, millel on järglasi
   class Node extends Tree {


      public final Tree leftChild;
      public final Tree rightChild;




      public Node( Tree leftChild, Tree rightChild) {
         super(leftChild.frequency + rightChild.frequency);
             this.leftChild = leftChild;
             this.rightChild = rightChild;



      }

   }

   public static void printByteArray(byte[]a){
      for(byte s:a){
         System.out.println (((int) s));
      }
   }


   /** Main method. */
   public static void main (String[] params) {
      String tekst = "AAAAAAAAAAAAABBBBBBCCCDDEEF";
      byte[] orig = tekst.getBytes();
      Huffman huf = new Huffman (orig);
      byte[] kood = huf.encode (orig);
      printByteArray(kood);
      byte[] orig2 = huf.decode (kood);
      // must be equal: orig, orig2
      System.out.println (Arrays.equals (orig, orig2));
      int lngth = huf.bitLength();
      System.out.println ("Length of encoded data in bits: " + lngth);
      // TODO!!! Your tests here!
   }

}

